/**---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
 * This file is part of the Neurorobotics Platform software
 * Copyright (C) 2014,2015,2016,2017 Human Brain Project
 * https://www.humanbrainproject.eu
 *
 * The Human Brain Project is a European Commission funded project
 * in the frame of the Horizon2020 FET Flagship plan.
 * http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * ---LICENSE-END**/
/*
 * Desc: Gazebo plugin providing generic controllers for robot joints
 * This plugin provides ROS topics to control single joints of the robot. The controlled joints can be specified in the SDF as plugin tags
 * Author: Lars Pfotzer
 */

#include "generic_controller_plugin.h"

#include <boost/bind.hpp>
#include <sensor_msgs/JointState.h>
#include <ros/time.h>
#include <geometry_msgs/Vector3.h>

namespace gazebo
{

GenericControlPlugin::GenericControlPlugin()
{
  m_nh = ros::NodeHandle();
}

GenericControlPlugin::~GenericControlPlugin()
{
  m_nh.shutdown();
}

void GenericControlPlugin::Load(physics::ModelPtr parent, sdf::ElementPtr sdf)
{
    m_model = parent;
    m_sdf = sdf;
    GetAllContactAreaSize();

    m_joint_controller = m_model->GetJointController();


    if (m_joint_controller){ // Null if there are no joints
    m_joints = m_joint_controller->GetJoints();

     // they are initial positon (same ad Mujoco start)
     this->m_joints["shadowhand_motor::rh_WRJ1"]->SetPosition(0,-0.16514339750464327);
     this->m_joints["shadowhand_motor::rh_WRJ0"]->SetPosition(0,-0.31973286565062153);

     this->m_joints["shadowhand_motor::rh_FFJ3"]->SetPosition(0, 0.14340512546557435);
     this->m_joints["shadowhand_motor::rh_FFJ2"]->SetPosition(0, 0.32028208333591573);
     this->m_joints["shadowhand_motor::rh_FFJ1"]->SetPosition(0, 0.7126053607727917);
     this->m_joints["shadowhand_motor::rh_FFJ0"]->SetPosition(0, 0.6705281001412586);

     this->m_joints["shadowhand_motor::rh_MFJ3"]->SetPosition(0, 0.000246444303701037);
     this->m_joints["shadowhand_motor::rh_MFJ2"]->SetPosition(0, 0.3152655251085491);
     this->m_joints["shadowhand_motor::rh_MFJ1"]->SetPosition(0, 0.7659800313729842);
     this->m_joints["shadowhand_motor::rh_MFJ0"]->SetPosition(0, 0.7323156897425923);

     this->m_joints["shadowhand_motor::rh_RFJ3"]->SetPosition(0, 0.00038520700007378114);
     this->m_joints["shadowhand_motor::rh_RFJ2"]->SetPosition(0, 0.36743546201985233);
     this->m_joints["shadowhand_motor::rh_RFJ1"]->SetPosition(0, 0.7119514095008576);
     this->m_joints["shadowhand_motor::rh_RFJ0"]->SetPosition(0, 0.6699446327514138);

     this->m_joints["shadowhand_motor::rh_LFJ4"]->SetPosition(0, 0.0525442258033891);
     this->m_joints["shadowhand_motor::rh_LFJ3"]->SetPosition(0, -0.13615534724474673);
     this->m_joints["shadowhand_motor::rh_LFJ2"]->SetPosition(0, 0.39872030433433003);
     this->m_joints["shadowhand_motor::rh_LFJ1"]->SetPosition(0, 0.7415570009679252);
     this->m_joints["shadowhand_motor::rh_LFJ0"]->SetPosition(0, 0.704096378652974);

     this->m_joints["shadowhand_motor::rh_THJ4"]->SetPosition(0, 0.003673823825070126);
     this->m_joints["shadowhand_motor::rh_THJ3"]->SetPosition(0, 0.5506291436028695);
     this->m_joints["shadowhand_motor::rh_THJ2"]->SetPosition(0, -0.014515151997119306);
     this->m_joints["shadowhand_motor::rh_THJ1"]->SetPosition(0, -0.0015229223564485414);
     this->m_joints["shadowhand_motor::rh_THJ0"]->SetPosition(0, -0.7894883021600622);
}





  //sdf::ElementPtr sdf->GetElement("");
  ROS_INFO("sdf name %s, sdf description %s", sdf->GetName().c_str(), sdf->GetDescription().c_str());


    for (JointMap::iterator joint_iter = m_joints.begin(); joint_iter != m_joints.end(); ++joint_iter)
  {
    physics::JointPtr joint = joint_iter->second;

    sdf::ElementPtr sdf_ctrl_def;

    // check, if controller for current joint was specified in the SDF and return sdf element pointer to controller
    if (existsControllerSDF(sdf_ctrl_def, sdf, joint))
    {
      // get controller parameter from sdf file
      common::PID ctrl_pid = getControllerPID(sdf_ctrl_def);
      std::string ctrl_type = getControllerType(sdf_ctrl_def);

      // create controller regarding the specified controller type
      if (ctrl_type == "position")
      {
        createPositionController(joint, ctrl_pid);
        
      }
      else if (ctrl_type == "velocity")
      {
        createVelocityController(joint, ctrl_pid);
      }
    }
  }

  // Controller time control.
  this->lastControllerUpdateTime = this->m_model->GetWorld()->SimTime();

  int numJoints = m_joints.size();
  m_js.header.stamp.sec = this->lastControllerUpdateTime.sec;
  m_js.header.stamp.nsec = this->lastControllerUpdateTime.nsec;
  m_js.name.resize ( numJoints );
  m_js.position.resize ( numJoints );
  m_js.velocity.resize ( numJoints );
  m_js.effort.resize ( numJoints );

  // Listen to the update event. This event is broadcast every simulation iteration.
  m_updateConnection = event::Events::ConnectBeforePhysicsUpdate(boost::bind(&GenericControlPlugin::OnUpdate, this, _1));

  const std::string joint_states_topic_name = m_model->GetName() + "/" + "joint_states";
  const std::string link_position_topic_name = m_model->GetName() + "/" + "link_positions";
  this->m_joint_state_pub = m_nh.advertise<sensor_msgs::JointState>( joint_states_topic_name, 10 );
  this->link_state_state_pub  = m_nh.advertise<shadow_hand_test::shadowhand_link_pose>( link_position_topic_name, 10 );


  this->shadow_hand_contact_force_pub  = m_nh.advertise<shadow_hand_test::shadow_hand_contact_force>( "shadow_hand_visual_tag_contact_sensor", 10 );



  m_setPIDParameterService = m_nh.advertiseService<
    SetPIDParameters::Request,
    SetPIDParameters::Response>(
      m_model->GetName() + "/set_pid_parameters",
      boost::bind(&GenericControlPlugin::setPIDParametersCB, this, _1, _2)
      
  );
    
  m_jointPropertiesService = m_nh.advertiseService<
    JointProperties::Request,
    JointProperties::Response>(
      m_model->GetName() + "/joint_properties",
      boost::bind(&GenericControlPlugin::getJointPropertiesCB, this, _1, _2)
  );

  m_ja.layout.dim.resize(1);
  m_ja.layout.dim[0].label = "accelerations";
  m_ja.layout.dim[0].size  = numJoints;
  m_ja.layout.dim[0].stride = 1;
  m_ja.data.resize(numJoints);

  const std::string joint_accel_topic_name = m_model->GetName() + "/" + "joint_accel";

  this->m_joint_accel_pub = m_nh.advertise<std_msgs::Float32MultiArray>( joint_accel_topic_name, 10 );
}

// Called by the world update start event
void GenericControlPlugin::OnUpdate(const common::UpdateInfo & /*_info*/)
{
  std::lock_guard<std::mutex> lock(this->mutex);

  gazebo::common::Time curTime = this->m_model->GetWorld()->SimTime();


  link_state.pose_array.poses.resize(this->m_model->GetLinks().size());
  link_state.link_name.resize(this->m_model->GetLinks().size());

  if (curTime > this->lastControllerUpdateTime)
  {
    m_js.header.stamp.sec = curTime.sec;
    m_js.header.stamp.nsec = curTime.nsec;

    link_state.pose_array.header.stamp.sec = curTime.sec;
    link_state.pose_array.header.stamp.nsec = curTime.nsec;



    // Update the control surfaces and publish the new state.
    int curr_ind = 0;
    for (JointMap::iterator joint_iter = m_joints.begin(); joint_iter != m_joints.end(); ++joint_iter, ++curr_ind)
    {
      physics::JointPtr joint = joint_iter->second;
      m_js.name[curr_ind] = joint->GetName();
      m_js.position[curr_ind] = joint->Position();
      m_js.velocity[curr_ind] = joint->GetVelocity(0);
      m_js.effort[curr_ind] = joint->GetForce(0);
      m_ja.data[curr_ind] = joint->GetAcceleration(0);
    }

      for (int i = 0 ; i < this->m_model->GetLinks().size(); i++) {

      //Get link name
      link_state.link_name[i] = this->m_model->GetLinks()[i]->GetName();
      //Get link position and orientation wrt World Frame
      link_state.pose_array.poses[i].position.x = this->m_model->GetLinks()[i]->WorldPose().Pos().X();
      link_state.pose_array.poses[i].position.y = this->m_model->GetLinks()[i]->WorldPose().Pos().Y();
      link_state.pose_array.poses[i].position.z = this->m_model->GetLinks()[i]->WorldPose().Pos().Z();
      link_state.pose_array.poses[i].orientation.x =this->m_model->GetLinks()[i]->WorldPose().Rot().X();
      link_state.pose_array.poses[i].orientation.y = this->m_model->GetLinks()[i]->WorldPose().Rot().Y();
      link_state.pose_array.poses[i].orientation.z = this->m_model->GetLinks()[i]->WorldPose().Rot().Z();
      link_state.pose_array.poses[i].orientation.w = this->m_model->GetLinks()[i]->WorldPose().Rot().W();

      ignition::math::Vector3d link_pos (this->m_model->GetLinks()[i]->WorldPose().Pos().X(),this->m_model->GetLinks()[i]->WorldPose().Pos().Y(),this->m_model->GetLinks()[i]->WorldPose().Pos().Z());
      ignition::math::Quaterniond link_quaternion (this->m_model->GetLinks()[i]->WorldPose().Rot().W(),this->m_model->GetLinks()[i]->WorldPose().Rot().X(),this->m_model->GetLinks()[i]->WorldPose().Rot().Y(),this->m_model->GetLinks()[i]->WorldPose().Rot().Z());
      ignition::math::Pose3d link_position_3d (link_pos,link_quaternion);

      link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()] = link_position_3d;

      for(std::map<std::string,ignition::math::Pose3d>::iterator it = link_based_visual_tag_pose3d_with_respect_to_its_link_frame[this->m_model->GetLinks()[i]->GetName()].begin(); it!= link_based_visual_tag_pose3d_with_respect_to_its_link_frame[this->m_model->GetLinks()[i]->GetName()].end(); ++it )
      {

              // visual_tag_position_wrt_world_frame = link_pose_wrt_world_frame x visual_tag_postion_wrt_link_frame (it->second)
              link_based_visual_tag_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()][it->first] = link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()] * it->second;
                /* This following lines are used to verify the positions,orientations and Pose3d multiplications [start from here] */
//              std::cout << "visual tag with respect to its link frame " << it->second << std::endl;
//              std::cout << "Link Position wrt world_frame : " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()] << std::endl;
//              std::cout << "Quaternion X : " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().X()<< std::endl;
//              std::cout << "Quaternion Y : " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().Y()<< std::endl;
//              std::cout << "Quaternion Z: " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().Z()<< std::endl;
//              std::cout << "Quaternion W: " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().W()<< std::endl;
//              std::cout << "Roll : " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().Roll()<< std::endl;
//              std::cout << "Pitch: " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().Pitch()<< std::endl;
//              std::cout << "Yaw " <<link_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()].Rot().Yaw()<< std::endl;
//              std::cout << link_based_visual_tag_pose3d_with_respect_to_world_frame[this->m_model->GetLinks()[i]->GetName()][it->first] << std::endl;
                /* [End here]*/



          }
      }



    m_joint_state_pub.publish ( m_js );
    m_joint_accel_pub.publish ( m_ja );

    link_state_state_pub.publish ( link_state );

    shadow_hand_contact_force_pub.publish(shadow_hand_contact_force_array);
    //clear the shadow_hand_contact_force_array  for every iteration update
    geometry_msgs::Vector3 zero_force;
    zero_force.x = 0;
    zero_force.y = 0;
    zero_force.z = 0;
    std::fill(shadow_hand_contact_force_array.force_array.begin(),shadow_hand_contact_force_array.force_array.end(),zero_force);
  }
  this->lastControllerUpdateTime = curTime;

}

void GenericControlPlugin::GetAllContactAreaSize(){

    sdf::ElementPtr contact_tags = this->m_sdf->GetElement("contact");
    // The while loop will collect the content of all <contact> tags and store them in contact_sensor_names map
    // contact_sensor_names maps the contact name to the geometry size
    while(contact_tags){
        std::string contact_tag_text = contact_tags->Get<std::string>();
        contact_tags = contact_tags->GetNextElement("contact");
        contact_sensor_names[contact_tag_text] = ignition::math::Vector3d(0,0,0);
    }




    // Get all links
    std::vector<physics::LinkPtr> all_model_links = this->m_model->GetLinks();

    for(int i =0 ; i < all_model_links.size() ; i++){
        sdf::ElementPtr link_sdf = all_model_links[i]->GetSDF();
        if (link_sdf -> HasElement("sensor")){
            sdf::ElementPtr sensor_tag_sdf = link_sdf->GetElement("sensor");
            if (sensor_tag_sdf -> HasElement("plugin")){
                sdf::ElementPtr plugin_tag_sdf = sensor_tag_sdf->GetElement("plugin");
                if (plugin_tag_sdf-> HasElement("bumperTopicName")){
                    sdf::ElementPtr bumper_topic_name = plugin_tag_sdf->GetElement("bumperTopicName");
                    std::string bumper_topic_name_text = bumper_topic_name->Get<std::string>();
                    /*For debuggin purposes [starts here] */
                    // std::cout << "bumper topic name :" << bumper_topic_name_text << std::endl;
                    /*For debuggin purposes [ends here] */

                    m_pos_sub_vec.push_back(m_nh.subscribe<gazebo_msgs::ContactsState>(bumper_topic_name_text, 1, boost::bind(&GenericControlPlugin::mesh_based_contact_sensor, this, _1, bumper_topic_name_text)));

                }
            }

        }
        else {
            ROS_WARN("No plugin tag is found under the sensor tag");
        }




    }


    for(int i = 0 ; i < all_model_links.size() ; i++ ) {
        sdf::ElementPtr link_sdf = all_model_links[i]->GetSDF();
        /*For debuggin purposes [starts here] */
        // std::cout << link_sdf->GetAttribute("name")->GetAsString() << std::endl;
        /*For debuggin purposes [ends here] */
        std::string link_name = link_sdf->GetAttribute("name")->GetAsString();
        if (link_sdf ->HasElement("visual")){
            // Getting  visual element
            sdf::ElementPtr visual_tag_sdf = link_sdf->GetElement("visual");
            while(visual_tag_sdf){
                //Getting visual tag pose3d with respect to its link frame (from sdf file)
                ignition::math::Pose3d visual_tag_position_wrt_link_frame = visual_tag_sdf->Get<ignition::math::Pose3d>("pose");
                // Getting multiple visual elements under the link tag if it exists
                if (visual_tag_sdf->HasAttribute("name")) {
                    // If attribute name is equal to the any contact name , then we can get the object size
                    std::string visual_tag_attribute_name =  visual_tag_sdf->GetAttribute("name")->GetAsString();

                    for (std::map<std::string, ignition::math::Vector3d>::iterator it = contact_sensor_names.begin();  it!=contact_sensor_names.end(); ++it ){
                        if ( visual_tag_attribute_name == it->first ){
                            /*For debuggin purposes [starts here] */
                            // std::cout << "visual tag name with contact: " << visual_tag_attribute_name << std::endl;
                            // std::cout << "Visual tag position wrt link frame: " << visual_tag_position_wrt_link_frame << std::endl;
                            /*For debuggin purposes [ends here] */
                            if (visual_tag_sdf->HasElement("geometry")){
                                sdf::ElementPtr visual_tag_geomerty_sdf = visual_tag_sdf->GetElement("geometry");
                                link_based_visual_tag_pose3d_with_respect_to_its_link_frame[link_name][visual_tag_attribute_name] = visual_tag_position_wrt_link_frame;
                                if(visual_tag_geomerty_sdf->HasElement("box"))
                                {
                                    sdf::ElementPtr visual_tag_geomerty_box_sdf = visual_tag_geomerty_sdf->GetElement("box");
                                    ignition::math::Vector3d box_size = visual_tag_geomerty_box_sdf->Get<ignition::math::Vector3d>("size");
                                    /*For debuggin purposes [starts here] */
                                    // std::cout << "box size: " << box_size << std::endl;
                                    /*For debuggin purposes [ends here] */
                                    contact_sensor_names[it->first] = box_size;
                                    link_based_contact_sensor_size[link_name][visual_tag_attribute_name] = box_size;
                                }


                                else if(visual_tag_geomerty_sdf->HasElement("sphere"))
                                {
                                    sdf::ElementPtr visual_tag_geomerty_box_sdf = visual_tag_geomerty_sdf->GetElement("sphere");
                                    double radius = visual_tag_geomerty_box_sdf->Get<double>("radius");
                                    ignition::math::Vector3d box_size(radius,radius,radius);
                                    /*For debuggin purposes [starts here] */
                                    // std::cout << "sphere: " << box_size << std::endl;
                                    /*For debuggin purposes [ends here] */
                                    contact_sensor_names[it->first] = box_size;
                                    link_based_contact_sensor_size[link_name][visual_tag_attribute_name] = box_size;
                                }
                                else{
                                    ROS_WARN("You cannot use any geometry except the box.");
                                }

                            }
                            else {
                                ROS_WARN("Visual tag does not have geometry");
                            }
                        }
                    }

                    visual_tag_sdf = visual_tag_sdf->GetNextElement("visual");
                }
            }
        }
    }



    shadow_hand_contact_force_array.contact_visual_tag_name.resize(contact_sensor_names.size());
    shadow_hand_contact_force_array.force_array.resize(contact_sensor_names.size());
    int visual_name_counter = 0;
    // Put all the visual tag names in array and publish them later
    for (std::map<std::string, ignition::math::Vector3d>::iterator it = contact_sensor_names.begin();  it!=contact_sensor_names.end(); ++it ,visual_name_counter++){

        shadow_hand_contact_force_array.contact_visual_tag_name[visual_name_counter] = it->first;

    }
}

///////////////////////////////////////// SDF parser functions ////////////////////////////////////////////

bool GenericControlPlugin::existsControllerSDF(sdf::ElementPtr &sdf_ctrl_def, const sdf::ElementPtr &sdf,
                                               const physics::JointPtr &joint)
{
  sdf::ElementPtr sdf_ctrl = sdf->GetElement("controller");
  while (sdf_ctrl != NULL)
  {
    sdf::ParamPtr joint_name_attr = sdf_ctrl->GetAttribute("joint_name");
    if (joint_name_attr != NULL)
    {
      std::string joint_name = joint_name_attr->GetAsString();
      if (joint_name == joint->GetName())
      {
        ROS_INFO("Found controller for joint %s", joint_name.c_str());
        sdf_ctrl_def = sdf_ctrl;
        return true;
      }
      else
      {
        // find next controller
        sdf_ctrl = sdf_ctrl->GetNextElement("controller");
      }
    }
    else
    {
      ROS_WARN("Attribute 'joint_name' is not available for current controller.");
      return false;
    }
  }

  ROS_WARN("No controller for joint %s found", joint->GetName().c_str());
  return false;
}

bool GenericControlPlugin::existsVisualSDF(sdf::ElementPtr& sdf_visual_def, const sdf::ElementPtr& sdf,
                                              const physics::JointPtr& joint)
{
  sdf::ElementPtr sdf_visual = sdf->GetElement("visual");
  while (sdf_visual != NULL)
  {
    sdf::ParamPtr joint_name_param = sdf_visual->GetAttribute("joint_name");
    if (joint_name_param != NULL)
    {
      std::string joint_name = joint_name_param->GetAsString();
      if (joint_name.compare(joint->GetName()) == 0)
      {
        ROS_INFO("Found visual properties for joint %s", joint_name.c_str());
        sdf_visual_def = sdf_visual;
        return true;
      }
      else
      {
        sdf_visual = sdf_visual->GetNextElement("visual");
      }
    }
    else
    {
      ROS_WARN("Attribute 'joint_name' is not available for current joint definition.");
      return false;
    }
  }

  return false;
}

common::PID GenericControlPlugin::getControllerPID(const sdf::ElementPtr &sdf_ctrl_def)
{
  common::PID pid_param;

  if (sdf_ctrl_def != NULL)
  {
    sdf::ElementPtr elem_pid = sdf_ctrl_def->GetElement("pid");
    if (elem_pid != NULL)
    {
#if SDF_MAJOR_VERSION > 3
      ignition::math::Vector3d pid_values = elem_pid->Get<ignition::math::Vector3d>();
      ROS_INFO("Controller PID values p=%f, i=%f, d=%f", pid_values.X(), pid_values.Y(), pid_values.Z());
      pid_param = common::PID(pid_values.X(), pid_values.Y(), pid_values.Z());
#else
      sdf::Vector3 pid_values = elem_pid->Get<sdf::Vector3>();
      ROS_INFO("Controller PID values p=%f, i=%f, d=%f", pid_values.x, pid_values.y, pid_values.z);
      pid_param = common::PID(pid_values.x, pid_values.y, pid_values.z);
#endif
      return pid_param;
    }
  }

  ROS_WARN("Could not find controller PID parameter in SDF file: Using default values.");
  pid_param = common::PID(1.0, 0.1, 0.01);
  return pid_param;
}

std::string GenericControlPlugin::getControllerType(const sdf::ElementPtr &sdf_ctrl_def)
{
  std::string ctrl_type = "";

  if (sdf_ctrl_def != NULL)
  {
    sdf::ElementPtr elem_type = sdf_ctrl_def->GetElement("type");
    if (elem_type != NULL)
    {
      ctrl_type = elem_type->Get<std::string>();
      ROS_INFO("Controller has type %s", ctrl_type.c_str());
      return ctrl_type;
    }
  }

  ROS_WARN("Could not find controller type in SDF file.");
  return ctrl_type;
}

//////////////////////////////////////// Controller construction //////////////////////////////////////////



void GenericControlPlugin::createPositionController(const physics::JointPtr &joint, const common::PID &pid_param)
{
  // generate joint topic name using the model name as prefix
  std::string name_prefix = m_model->GetName() + "/" + joint->GetName();
  replace(name_prefix.begin(), name_prefix.end(), ':', '_');

  const std::string topic_name = name_prefix + "/cmd_pos";
  const std::string set_service_name = name_prefix + "/set_target";
  const std::string get_service_name = name_prefix + "/get_target";
  const std::string get_pid_name = name_prefix + "/get_pid";
  const std::string set_pid_name = name_prefix + "/get_pid";

  // Add ROS topic for position control
  m_pos_sub_vec.push_back(m_nh.subscribe<std_msgs::Float64>(topic_name, 1,
                                                            boost::bind(&GenericControlPlugin::positionCB, this, _1, joint)));




  // Add ROS service for position control
  m_pos_service_vec.push_back(m_nh.advertiseService<gazebo_msgs::GetJointStates::Request, gazebo_msgs::GetJointStates::Response>(get_service_name,
                                                                                 boost::bind(&GenericControlPlugin::getPositionServiceCB, this, _1, _2, joint)));

  m_pos_service_vec.push_back(m_nh.advertiseService<gazebo_msgs::SetJointStates::Request, gazebo_msgs::SetJointStates::Response>(set_service_name,
                                                                                 boost::bind(&GenericControlPlugin::setPositionServiceCB, this, _1, _2, joint)));

  //  Here is erdisayar implementing rosservice for PID values

  // m_pid_service_vec.push_back(m_nh.advertiseService<gazebo_msgs::SetJointStates::Request, gazebo_msgs::SetJointStates::Response>(set_pid_name,
  //                                                                                boost::bind(&GenericControlPlugin::setPositionServiceCB, this, _1, _2, joint)))

  // m_pid_service_vec.push_back(m_nh.advertiseService<gazebo_msgs::SetJointStates::Request, gazebo_msgs::SetJointStates::Response>(get_pid_name,
  //                                                                                boost::bind(&GenericControlPlugin::setPositionServiceCB, this, _1, _2, joint)))


  //  Implementation ends here

  // Store information of Actuator in Model class
  m_model->SaveControllerActuatorRosTopics(topic_name, "std_msgs/Float64");

  // Create PID parameter for position controller
  m_joint_controller->SetPositionPID(joint->GetScopedName(), pid_param);

  // Initialize controller with zero position
  // m_joint_controller->SetPositionTarget(joint->GetScopedName(), 0.0);
  m_joint_controller->SetPositionTarget(joint->GetScopedName(), initial_joint_pos[joint->GetScopedName()]);

  ROS_INFO("Added new position controller for joint %s", joint->GetName().c_str());
}

void GenericControlPlugin::createVelocityController(const physics::JointPtr &joint, const common::PID &pid_param)
{
  // generate joint topic name using the model name as prefix
  std::string name_prefix = m_model->GetName() + "/" + joint->GetName();
  replace(name_prefix.begin(), name_prefix.end(), ':', '_');

  const std::string topic_name = name_prefix + "/cmd_pos";
  const std::string set_service_name = name_prefix + "/set_target";
  const std::string get_service_name = name_prefix + "/get_target";

  // Add ROS topic for velocity control
  m_vel_sub_vec.push_back(m_nh.subscribe<std_msgs::Float64>(topic_name, 1,
                                                            boost::bind(&GenericControlPlugin::velocityCB, this, _1, joint)));

  // Add ROS service for position control
  m_vel_service_vec.push_back(m_nh.advertiseService<gazebo_msgs::GetJointStates::Request, gazebo_msgs::GetJointStates::Response>(get_service_name,
                                                                                 boost::bind(&GenericControlPlugin::getVelocityServiceCB, this, _1, _2, joint)));

  m_vel_service_vec.push_back(m_nh.advertiseService<gazebo_msgs::SetJointStates::Request, gazebo_msgs::SetJointStates::Response>(set_service_name,
                                                                                 boost::bind(&GenericControlPlugin::setVelocityServiceCB, this, _1, _2, joint)));

  // Store information of Actuator in Model class
  m_model->SaveControllerActuatorRosTopics(topic_name, "std_msgs/Float64");

  // Create PID parameter for velocity controller
  m_joint_controller->SetVelocityPID(joint->GetScopedName(), pid_param);

  // Initialize controller with zero velocity
  m_joint_controller->SetVelocityTarget(joint->GetScopedName(), 0.0);

  ROS_INFO("Added new velocity controller for joint %s", joint->GetName().c_str());
}

//////////////////////////////////////// ROS topic callback functions //////////////////////////////////////////

void GenericControlPlugin::positionCB(const std_msgs::Float64::ConstPtr &msg, const physics::JointPtr &joint)
{
  ROS_DEBUG("positionCB called! Joint name = %s, joint pos = %f", joint->GetName().c_str(), msg->data);


  double angle_rad(msg->data);
  m_joint_controller->SetPositionTarget(joint->GetScopedName(), angle_rad);
  //pid.SetCmd(angle_rad);
}

void GenericControlPlugin::mesh_based_contact_sensor(const gazebo_msgs::ContactsState::ConstPtr& msg, std::string topic_name)
{
 if (msg->states.size() > 0){

     // Assume that the sensor topic names contacts/rh_ff/knuckle in this format and extract only rh_ffknuckle which corresponds to the link name of this sensor.
     // Note: Current_sensor_topic_name should match with the link_name
     std::string current_sensor_topic_name =  topic_name.substr(9,5) + topic_name.substr(15,topic_name.size()) ;
     /*For debuggin purposes [starts here] */
        // std::cout << topic_name.substr(9,5) +topic_name.substr(15,topic_name.size())  << std::endl;
        // std::cout << contact_sensor_names[topic_name] << std::endl;
      // std::cout << msg->states[0].collision1_name << std::endl;
      // std::cout << msg->states[0].wrenches.size()<< std::endl;
      /*For debuggin purposes [ends here] */

     for (int i = 0 ; i < msg->states.size(); i++)
     {
        // std::cout << "You are in state: " << i << std::endl;

         for ( int j =0 ; j < msg->states[i].wrenches.size(); j++){

            /*For debuggin purposes [starts here] */
            // std::cout << "You are in wrench " << j  << std::endl;

            // std::cout << "the force " << msg->states[i].wrenches[j].force.x << std::endl;
            // std::cout << "the contact position x " << msg->states[i].contact_positions[j].x << std::endl;
            /*For debuggin purposes [ends here] */


             int visual_tag_counter = 0;

             for (std::map<std::string, ignition::math::Vector3d>::iterator it = link_based_contact_sensor_size[current_sensor_topic_name].begin(); it != link_based_contact_sensor_size[current_sensor_topic_name].end(); it++, ++visual_tag_counter ){



                 ignition::math::Vector3d contact_sensor_bounding_box_max = link_based_visual_tag_pose3d_with_respect_to_world_frame[current_sensor_topic_name][it->first].Pos()  + (link_based_contact_sensor_size[current_sensor_topic_name][it->first] * 0.5) ;
                 ignition::math::Vector3d contact_sensor_bounding_box_min = link_based_visual_tag_pose3d_with_respect_to_world_frame[current_sensor_topic_name][it->first].Pos()  - (link_based_contact_sensor_size[current_sensor_topic_name][it->first] * 0.5) ;

                 /*For debuggin purposes [starts here] */
                // std::cout << "contact sensor bounding box max : " << contact_sensor_bounding_box_max << std::endl;
                // std::cout << "contact sensor bounding box min : " << contact_sensor_bounding_box_min << std::endl;
                // std::cout << "visual tag size: " <<  link_based_contact_sensor_size[current_sensor_topic_name][it->first] << std::endl;
                //  visual_tag position with respect to world (absolute) frame
                // std::cout << it->first << ": " << link_based_visual_tag_pose3d_with_respect_to_world_frame[current_sensor_topic_name][it->first] << std::endl;
                /*For debuggin purposes [ends here] */


                 // If contact position inside visual_tag absolute position + its geomerty bounding box,

                 if (msg->states[i].contact_positions[j].x <contact_sensor_bounding_box_max.X()  and msg->states[i].contact_positions[j].x > contact_sensor_bounding_box_min.X() and
                     msg->states[i].contact_positions[j].y <contact_sensor_bounding_box_max.Y()  and msg->states[i].contact_positions[j].y > contact_sensor_bounding_box_min.Y() and
                    msg->states[i].contact_positions[j].z <contact_sensor_bounding_box_max.Z()  and msg->states[i].contact_positions[j].z > contact_sensor_bounding_box_min.Z()) {
                      
                     /*For debuggin purposes [starts here] */
                    //  std::cout << "There is contact point in "  << j  << ": " << it->first << std::endl;
                    //  std::cout <<  "size:" << link_based_contact_sensor_size[current_sensor_topic_name].size() << std::endl;
                    //  std::cout << visual_tag_counter << std::endl;
                    //  std::cout << "force:" << msg->states[i].wrenches[j].force << std::endl;
                     /*For debuggin purposes [ends here] */

                     // Search it-first over  shadow_hand_contact_force_array.contact_visual_tag_name to find the index and use this index number for force value
                    std::vector<std::string>::iterator visual_tag_iter = std::find(shadow_hand_contact_force_array.contact_visual_tag_name.begin(),shadow_hand_contact_force_array.contact_visual_tag_name.end(), it->first);
                    if (visual_tag_iter != shadow_hand_contact_force_array.contact_visual_tag_name.end()) {

                        // std::cout << "Element found: " << it->first << std::endl;

                        int found_index =  visual_tag_iter - shadow_hand_contact_force_array.contact_visual_tag_name.begin();
                        // std::cout << "Index: " <<  found_index << std::endl;

                        shadow_hand_contact_force_array.contact_visual_tag_name[found_index] == it->first;
                        geometry_msgs::Vector3 contact_resolution_force;

                        //Resolve XYZ forces wrt to word frame in contact normal direction
                        contact_resolution_force.x = msg->states[i].wrenches[j].force.x * msg->states[i].contact_normals[j].x;
                        contact_resolution_force.y = msg->states[i].wrenches[j].force.y * msg->states[i].contact_normals[j].y;
                        contact_resolution_force.z = msg->states[i].wrenches[j].force.z * msg->states[i].contact_normals[j].z;

//                        Publishing the Forces with respect to world frame
//                        shadow_hand_contact_force_array.force_array[found_index] = msg->states[i].wrenches[j].force;
                        shadow_hand_contact_force_array.force_array[found_index] = contact_resolution_force;

                   }
                 } //end of if condition for boundary box checking
             }
         }
     }
 }

}

void GenericControlPlugin::velocityCB(const std_msgs::Float64::ConstPtr &msg, const physics::JointPtr &joint)
{
  ROS_DEBUG("velocityCB called! Joint name = %s, joint vel = %f", joint->GetName().c_str(), msg->data);
  double velocity_m_per_sec(msg->data);
  m_joint_controller->SetVelocityTarget(joint->GetScopedName(), velocity_m_per_sec);
  //pid.SetCmd(velocity_m_per_sec);
}

bool GenericControlPlugin::getPositionServiceCB(const gazebo_msgs::GetJointStates::Request &req, gazebo_msgs::GetJointStates::Response &res, const physics::JointPtr &joint)
{
  return getGeneralServiceCB(req, res, joint, m_joint_controller->GetPositions());
}

bool GenericControlPlugin::getVelocityServiceCB(const gazebo_msgs::GetJointStates::Request &req, gazebo_msgs::GetJointStates::Response &res, const physics::JointPtr &joint)
{
  return getGeneralServiceCB(req, res, joint, m_joint_controller->GetVelocities());
}

bool GenericControlPlugin::setPositionServiceCB(const gazebo_msgs::SetJointStates::Request &req, gazebo_msgs::SetJointStates::Response &res, const physics::JointPtr &joint)
{
  res.success = m_joint_controller->SetPositionTarget(joint->GetScopedName(), req.value.at(0));
  if(!res.success)
    res.status_message = "Could not set position target for joint " + joint->GetScopedName();

  return res.success;
}

bool GenericControlPlugin::setVelocityServiceCB(const gazebo_msgs::SetJointStates::Request &req, gazebo_msgs::SetJointStates::Response &res, const physics::JointPtr &joint)
{
  res.success = m_joint_controller->SetVelocityTarget(joint->GetScopedName(), req.value.at(0));
  if(!res.success)
    res.status_message = "Could not set velocity target for joint " + joint->GetScopedName();

  return res.success;
}

bool GenericControlPlugin::setPIDParametersCB(SetPIDParameters::Request &req,
                                              SetPIDParameters::Response &res)
{
  std::string joint_name = req.joint;
  double kp = req.kp,
         ki = req.ki,
         kd = req.kd;
  ROS_DEBUG("setPIDParametersCB called! Joint name = %s, kp = %f, ki = %f, kd = %f", joint_name.c_str(), kp, ki, kd);

  JointMap::iterator it = m_joints.find(joint_name);
  if (it == m_joints.end())
  {
    res.success = false;
    return false;
  }
  else
  {
    // We set same parameters for position and velocity PID. That is because
    // we do not know which kind of controller is used. At the time of this
    // writing this just puts the parameters in a map of the controller manager.
    // Parameters are obtained from there when the force is calculated.
    const std::string joint_id = it->second->GetScopedName();
    m_joint_controller->SetPositionPID(joint_id, common::PID(kp, ki, kd));
    m_joint_controller->SetVelocityPID(joint_id, common::PID(kp, ki, kd));
    res.success = true;
    return true;
  }
}

bool GenericControlPlugin::getJointPropertiesCB(JointProperties::Request &req,
                             JointProperties::Response &res)
{
  if (m_joints.size() > 0)
  {
    res.joint.resize(m_joints.size());
    res.lower_limit.resize(m_joints.size());
    res.upper_limit.resize(m_joints.size());
    int jointIdx = 0;
    for (JointMap::iterator joint_iter = m_joints.begin(); joint_iter != m_joints.end(); ++joint_iter)
    {
        physics::JointPtr joint = joint_iter->second;
        res.joint[jointIdx] = joint->GetName();
        
        // This only gives you the limits of the first joint axis/degree of freedom!
        res.lower_limit[jointIdx] = joint->LowerLimit(0);
        res.upper_limit[jointIdx] = joint->UpperLimit(0);
        jointIdx++;
    }
  }
  return true;
}
// Register this plugin with the simulator
GZ_REGISTER_MODEL_PLUGIN(GenericControlPlugin)

} // namespace gazebo
