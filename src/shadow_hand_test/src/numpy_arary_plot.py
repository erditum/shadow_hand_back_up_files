#!/usr/bin/env python
import rospkg
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    rospack = rospkg.RosPack()
    path_of_shadow_hand_package = rospack.get_path('shadow_hand_test')

    total_force_array = np.load(path_of_shadow_hand_package+"/data/total_force_array.npy")
    force_differece_array = np.load(path_of_shadow_hand_package+"/data/force_difference_array.npy")
    timesteps = np.load(path_of_shadow_hand_package+"/data/timesteps.npy")


    start_index = 100
    end_index = 600

    plt.xlabel('Time Step')
    plt.ylabel('Force Difference') 
    plt.plot(timesteps[start_index:end_index],force_differece_array[start_index:end_index],color="black")
    plt.show()