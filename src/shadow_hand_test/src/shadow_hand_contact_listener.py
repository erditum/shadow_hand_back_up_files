#!/usr/bin/env python
import rospy
import rospkg
from gazebo_msgs.msg import ContactsState
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

total_force_previous = 0
number_of_contact = 0
force_difference = 0
counter = 0
force_array = np.array([])

def callback(data):
    global total_force_previous
    global number_of_contact
    global force_difference
    global counter
    global force_array
    global timestep_array,force_difference_array
    total_force = 0
    counter+=1
    print("Contact number: " , len(data.states))
    for i in range(len(data.states)):
        # print(data.states[i].collision2_name)
        for j in range(len(data.states[i].wrenches)):
            total_force += data.states[i].wrenches[j].force.z + data.states[i].wrenches[j].force.x + data.states[i].wrenches[j].force.y
    

    print("Total force: ",total_force)
    force_difference = total_force - total_force_previous
    print("Difference previous from force: ", force_difference)
    total_force_previous = total_force
    number_of_contact = len(data.states)
    print("Timestep:",counter)

    force_array = np.append(force_array,total_force)
    timestep_array = np.append(timestep_array,counter)
    force_difference_array = np.append(force_difference_array,force_difference)
    np.save(path_of_shadow_hand_package+"/data/total_force_array",force_array)
    np.save(path_of_shadow_hand_package+"/data/force_difference_array",force_difference_array)
    np.save(path_of_shadow_hand_package+"/data/timesteps",timestep_array)
    

    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('shadow_hand_contact_listener', anonymous=True)
    rospy.Subscriber("/gazebo/contact_point_data", ContactsState, callback)
    # rate = rospy.Rate(1) # 10hz
    # rate.sleep()

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    rospack = rospkg.RosPack()
    path_of_shadow_hand_package = rospack.get_path('shadow_hand_test')
    timestep_array = np.array([])
    force_difference_array = np.array([])
    # print(path)
    listener()
