#include <functional>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ros/ros.h>


namespace gazebo
{
  class ShadowHand : public ModelPlugin
  {
    public:

      ShadowHand(){
          std::cout << "Shadowhand constructor is called " << std::endl;

      }

      ~ShadowHand(){
          std::cout << "Shadowhand destructor is called " << std::endl;
      }
    
    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
      this->model = _parent;
      this->joint_controller = this->model->GetJointController();
      ROS_WARN("Shadow hand  model plugin is loaded for %s", this->model->GetName().c_str());
      this->joints = this->joint_controller->GetJoints();
      ROS_WARN("number of joint of %s is %lu", this->model->GetName().c_str(), joints.size());

      //sdf::ElementPtr sdf_ctrl = _sdf->GetElement("model");
      std::cout << _sdf->ToString("") << std::endl;


      sdf::ElementPtr sdf_ctrl = _sdf->GetElement("controller");
      std::cout << sdf_ctrl->ToString("") << std::endl;
      sdf::ParamPtr joint_name_attr = sdf_ctrl->GetAttribute("joint_name");
      std::string joint_name = joint_name_attr->GetAsString();
      std::cout << "joint name " << joint_name << std::endl;


        // Initial position parameters (the values are taken from the gym environment reach.py script)

        this->joints["shadowhand_motor::rh_WRJ1"]->SetPosition(0,-0.16514339750464327);
        this->joints["shadowhand_motor::rh_WRJ0"]->SetPosition(0,-0.31973286565062153);

        this->joints["shadowhand_motor::rh_FFJ3"]->SetPosition(0, 0.14340512546557435);
        this->joints["shadowhand_motor::rh_FFJ2"]->SetPosition(0, 0.32028208333591573);
        this->joints["shadowhand_motor::rh_FFJ1"]->SetPosition(0, 0.7126053607727917);
        this->joints["shadowhand_motor::rh_FFJ0"]->SetPosition(0, 0.6705281001412586);

        this->joints["shadowhand_motor::rh_MFJ3"]->SetPosition(0, 0.000246444303701037);
        this->joints["shadowhand_motor::rh_MFJ2"]->SetPosition(0, 0.3152655251085491);
        this->joints["shadowhand_motor::rh_MFJ1"]->SetPosition(0, 0.7659800313729842);
        this->joints["shadowhand_motor::rh_MFJ0"]->SetPosition(0, 0.7323156897425923);

        this->joints["shadowhand_motor::rh_RFJ3"]->SetPosition(0, 0.00038520700007378114);
        this->joints["shadowhand_motor::rh_RFJ2"]->SetPosition(0, 0.36743546201985233);
        this->joints["shadowhand_motor::rh_RFJ1"]->SetPosition(0, 0.7119514095008576);
        this->joints["shadowhand_motor::rh_RFJ0"]->SetPosition(0, 0.6699446327514138);

        this->joints["shadowhand_motor::rh_LFJ4"]->SetPosition(0, 0.0525442258033891);
        this->joints["shadowhand_motor::rh_LFJ3"]->SetPosition(0, -0.13615534724474673);
        this->joints["shadowhand_motor::rh_LFJ2"]->SetPosition(0, 0.39872030433433003);
        this->joints["shadowhand_motor::rh_LFJ1"]->SetPosition(0, 0.7415570009679252);
        this->joints["shadowhand_motor::rh_LFJ0"]->SetPosition(0, 0.704096378652974);

        this->joints["shadowhand_motor::rh_THJ4"]->SetPosition(0, 0.003673823825070126);
        this->joints["shadowhand_motor::rh_THJ3"]->SetPosition(0, 0.5506291436028695);
        this->joints["shadowhand_motor::rh_THJ2"]->SetPosition(0, -0.014515151997119306);
        this->joints["shadowhand_motor::rh_THJ1"]->SetPosition(0, -0.0015229223564485414);
        this->joints["shadowhand_motor::rh_THJ0"]->SetPosition(0, -0.7894883021600622);


      for(std::map<std::string, physics::JointPtr>::iterator each_joint =joints.begin() ; each_joint!= joints.end(); each_joint++  ){

          std::cout << each_joint->first << std::endl;
         //each_joint->second->SetPosition(0,0.5);
          std::cout << each_joint->second->Position(0) << std::endl;

      }














    }


    private:
        physics::ModelPtr model;
        physics::JointControllerPtr joint_controller;
        std::map<std::string, physics::JointPtr> joints;



  };
  GZ_REGISTER_MODEL_PLUGIN(ShadowHand)
}

