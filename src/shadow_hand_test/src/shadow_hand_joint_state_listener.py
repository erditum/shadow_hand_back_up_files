#!/usr/bin/env python
import rospy
from sensor_msgs.msg import JointState
from std_msgs.msg import Float64
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
joint_state = 0


def callback(data):
    global joint_state
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.name[0])
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.position[0])
    index_number = data.name.index('rh_MFJ2')
    joint_state = data.position[index_number]

    # print(joint_state)
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('joint_state_listener', anonymous=True)

    x_vals = []
    y_vals = []
    counter = 0 
    rospy.Subscriber("/shadowhand_motor/joint_states", JointState, callback)
    pub = rospy.Publisher('/shadow_hand_pid_parameter_test', Float64, queue_size=10)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rospy.loginfo(joint_state)
        pub.publish(joint_state)
        counter = counter + 1
        y_vals.append(joint_state)
        x_vals.append(counter)
        plt.plot(x_vals,y_vals)
        if counter % 100 == 0:
            plt.show(block=True)
        rate.sleep()

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
