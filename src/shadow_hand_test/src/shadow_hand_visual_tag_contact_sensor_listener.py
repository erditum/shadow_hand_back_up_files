#!/usr/bin/env python
import rospy
import rospkg
from shadow_hand_test.msg import shadow_hand_contact_force
import numpy as np
def callback(data):
    global contact_names
    if not contact_names:
        contact_names = {key: [] for key in data.contact_visual_tag_name}


    for c_n in data.contact_visual_tag_name:
        index_c_n = list(contact_names).index(c_n)
        contact_names[c_n].append(data.force_array[index_c_n].x+data.force_array[index_c_n].y+data.force_array[index_c_n].z)


    # print(data)
    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('shadow_hand_visual_tag_contact_sensor_listener', anonymous=True)

    counter = 0 
    rospy.Subscriber("shadow_hand_visual_tag_contact_sensor", shadow_hand_contact_force, callback)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rate.sleep()
        np.save(path_of_shadow_hand_package+"/data/visual_tag_low_pass_filter/contact_names.npy",contact_names)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    rospack = rospkg.RosPack()
    path_of_shadow_hand_package = rospack.get_path('shadow_hand_test')
    contact_names = {}
    listener()
