
```bash
roslaunch shadow_hand_test shadow_hand_gazebo.launch
```

This will start shadow hand using the .sdf file

```bash
roslaunch shadow_hand_test main.launch
```

It will start the shadow hand using urdf description without activating the controller

```bash
roslaunch shadow_hand_test shadow_hand_controllers.launch
```

You can activate the controller using above command after you start the shadow hand.

## Model Architrecture![Model Architecture](https://i.imgur.com/aGGiY2j.png)

Model input
Number 1;
The model has 24 revolute joints. The position value of each joint can be given using ros services. 
For example 
` rosservice call /shadowhand_motor/rh_RFJ2/set_target "value: - 0.5"`  will set   0.5 to the ring finger .

Model outputs
Number 2;
The mass center position of the links can be obtained via rostopics
For example;
`rostopic echo /shadowhand_motor/link_positions `

Number3;
The contact information between object and hand via ros topic
For example;
`rostopic echo /contacts/rh_mf/proximal` 
Number4;
The pixel values from the camera can be obtained via ros topic 
rostopic echo /shadow_hand/camera/image_raw
